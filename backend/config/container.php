<?php

use Illuminate\Container\Container as IlluminateContainer;
use Illuminate\Database\Connection;
use Illuminate\Database\Connectors\ConnectionFactory;
use App\Auth\JwtAuth;
use App\Factory\LoggerFactory;
use App\Middleware\TranslatorMiddleware;
use Illuminate\Translation\FileLoader;
use Illuminate\Validation\Factory;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseFactoryInterface;
use Selective\Config\Configuration;
use Selective\Validation\Encoder\JsonEncoder;
use Selective\Validation\Middleware\ValidationExceptionMiddleware;
use Slim\App;
use Slim\Factory\AppFactory;
use Symfony\Component\Translation\Formatter\MessageFormatter;
use Symfony\Component\Translation\IdentityTranslator;
use Symfony\Component\Translation\Loader\MoFileLoader;
use Symfony\Component\Translation\Translator;

return [
    //settings
    Configuration::class => function () {
        return new Configuration(require __DIR__ . '/settings.php');
    },
    //container loading
    App::class => function (ContainerInterface $container) {
        AppFactory::setContainer($container);
        
        return AppFactory::create();
    },
    //response factory
    ResponseFactoryInterface::class => function (ContainerInterface $container) {
        return $container->get(App::class)->getResponseFactory();
    },
    // The logger factory
    LoggerFactory::class => function (ContainerInterface $container) {
        return new LoggerFactory($container->get(Configuration::class)->getArray('logger'));
    },
    Connection::class => function (ContainerInterface $container) {
        $factory = new ConnectionFactory(new IlluminateContainer());
        
        $connection = $factory->make($container->get(Configuration::class)->getArray('db'));
        
        // Disable the query log to prevent memory issues
        $connection->disableQueryLog();
        
        return $connection;
    },
    PDO::class => function (ContainerInterface $container) {
        /** @var \Illuminate\Database\Connection $db */
        $db = $container->get(Connection::class);
        return $db->getPdo();
    },
    JwtAuth::class => function (ContainerInterface $container) {
        $config = $container->get(Configuration::class);
        $issuer = $config->getString('jwt.issuer');
        $lifetime = $config->getInt('jwt.lifetime');
        $privateKey = $config->getString('jwt.private_key');
        $publicKey = $config->getString('jwt.public_key');
        
        return new JwtAuth($issuer, $lifetime, $privateKey, $publicKey);
    },
    Factory::class => function () {
        $fileSystem = new Illuminate\Filesystem\Filesystem();
        $loader = new FileLoader($fileSystem, __DIR__ . '/../lang');
        $loader->addNamespace('lang', __DIR__ . '/../lang');
        $loader->load($lang = 'en', $group = 'validation', $namespace = 'lang');
        $factory = new Illuminate\Translation\Translator($loader, 'en');
        
        return new Illuminate\Validation\Factory($factory);
    },
    ValidationExceptionMiddleware::class => function (ContainerInterface $container) {
        $factory = $container->get(ResponseFactoryInterface::class);
        
        return new ValidationExceptionMiddleware($factory, new JsonEncoder());
    },
];
