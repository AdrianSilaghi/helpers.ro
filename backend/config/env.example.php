<?php

/**
 * Environment specific application configuration.
 *
 * You should store all secret information (username, password, token) here.
 *
 * Make sure the env.php file is added to your .gitignore
 * so it is not checked-in the code
 *
 * Place the env.php _outside_ the project root directory, to protect against
 * overwriting at deployment.
 *
 * This usage ensures that no sensitive passwords or API keys will
 * ever be in the version control history so there is less risk of
 * a security breach, and production values will never have to be
 * shared with all project collaborators.
 */
require __DIR__ . '/development.php';

// Database
$settings['db']['username'] = 'root';
$settings['db']['password'] = '';
$settings['jwt'] = [
    'issuer' => 'www.helpers.ro',
    'lifetime' => 14400,
    'private_key' => '-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAw2nL6/hR2ZP4waWzK7xooo8H76dSpAoY3FMlW7ITmXSQXbHg
wVFlYBzII0ObEPun/NnUaaHcJU6OLRiDZlArCtVy8Qd9iWE6hvsuV3uyvZcxahBl
Z4g6B7TyHYHBw7eZerjdy7YE6AKmrMMbjuWmSBDdt4DjnLoe3BRLEXadtLSuDA3/
LP9Xy85C+FdHuNo09txdYVsTnNB1ZoczaLiuhSUoxmht/YAuCrckthQPXvHt2vpS
T8fcHT69oGizPOG3iPLFJ+2KgjAaad2/2WDR4OR7KLOHuYQcIZeQmIwGHvR1jVal
MBm0J4dwqeEL0HQCtoRcqVdIfGphvRpUTZ2qqwIDAQABAoIBAA75ZnT4nF0N57v/
FJG/aE08pKEKs1x6oZkQjgZOyQ2zTvS/Erf9osUQr410Ose8YQ0RLCPEEXiSYna2
MCuA7StxW9N4fXgviSqayNxPUE0S7uhvxTV/zOKWTQ9RyCVVRl+PFv/rPMkCmv8W
HGCA3JMdJ8BOEiMKaB75Nx9pdYBJTcwijiwTvEbkkYiyzKbnY72idYTHqm5/sKvt
M4PZO5Oec7qPyJaDVkP0Vrkdl+w7xgrR08x/yYSv/SwkLwR70Wwg+/m/M5MHO/ta
m5j/UbMOeqvJDciDNbqKkms8nqOTLvnH3xq3RJCi98eVh7hZ5KiABleMmEZ06P8o
u1lm7rECgYEA7kKLTw9eD+MAlWe4k7pcqW7cfInN1E3p3y+V0doyEy36stdAgCc1
BGfk8yu6KRkHuzmh85UiwcQoWQAc3aWIBDZu3Jw1BCwt7vet5UXF89TiLh/ReTW9
0bDub4WXXIokRurUY/RYeokfdkvLtvOPrHtt4/AhNnKI/dsNztgV6R0CgYEA0faO
F0IiISLFS9S03BN9gx54OV/nATYezeZOSYtt3KXe8qvKe6Lp9pgv9EuiibXFYRcA
91Vto/VNWh5XS7gIwX8MHDKoP0OdsjIholHMr6PON7afTO5dePFO2m50hA2iethl
3gE3ODJCcEwSt1qeJI93/wPk0vqhFjFHbF9KYGcCgYByEFt/Iv1GH8f0vu4nky2v
U8qsuJBYlmBjn/VavNoCQp3lppVThPGlxVNfc0yxRjFkWagF5Bz6pL0oNpybJBck
5SZX6VScRcZLxDl1LN0i6YTKziLLo4XXcJwFufAHtn69kMjTfXQqXaE5UfoaASqE
SacHW7uaxGR/G7LVlq+OpQKBgQCk/6reIDyN6bM+SmPu5uolllZIUFNLImrGyAwR
w56MXVV9Qv+WdYqDHUniI2YR7mS0Tc2WkO3egTFOdrWHbZKD7QFpT23keZB9ruO7
majPBl1SbxN084VFO+0Jvu+VrtlWzKQQ+MeaQ/TuJvo2WOIaPRzBrR+vpQHeXM+S
HLhoVQKBgCuvNn54mvKgD2kTjyvGnfDdfokJObHu8nU6xz41ETfMPxaCT9f4xMOG
9oLRbFdzz/TjokGo1VnNY/bbSdR5WsC5WS97+t3sgrOzhCadb/RXISxWGHoU0HDv
Qg+qhWNHRF1IyLGuP5TQg4yyiVu+uSJ6WaNu1xX3kMIaWAYxlRDU
-----END RSA PRIVATE KEY-----',
    'public_key' => '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAw2nL6/hR2ZP4waWzK7xo
oo8H76dSpAoY3FMlW7ITmXSQXbHgwVFlYBzII0ObEPun/NnUaaHcJU6OLRiDZlAr
CtVy8Qd9iWE6hvsuV3uyvZcxahBlZ4g6B7TyHYHBw7eZerjdy7YE6AKmrMMbjuWm
SBDdt4DjnLoe3BRLEXadtLSuDA3/LP9Xy85C+FdHuNo09txdYVsTnNB1ZoczaLiu
hSUoxmht/YAuCrckthQPXvHt2vpST8fcHT69oGizPOG3iPLFJ+2KgjAaad2/2WDR
4OR7KLOHuYQcIZeQmIwGHvR1jValMBm0J4dwqeEL0HQCtoRcqVdIfGphvRpUTZ2q
qwIDAQAB
-----END PUBLIC KEY-----',
];
