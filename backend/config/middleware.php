<?php

use App\Handler\JsonErrorRenderer;
use App\Middleware\CorsMiddleware;
use App\Middleware\JwtMiddleware;
use Selective\Config\Configuration;
use Selective\Validation\Middleware\ValidationExceptionMiddleware;
use Slim\App;

return function (App $app) {
	// Parse json, form data and xml
	$app->addBodyParsingMiddleware();
    $app->add(CorsMiddleware::class);

	// Add routing middleware
	$app->addRoutingMiddleware();
	$app->add(ValidationExceptionMiddleware::class);

	$container = $app->getContainer();
//
	// Error handler
	$settings = $container->get(Configuration::class)->getArray('error_handler_middleware');
	$displayErrorDetails = (bool)$settings['display_error_details'];
	$logErrors = (bool)$settings['log_errors'];
	$logErrorDetails = (bool)$settings['log_error_details'];

	$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, $logErrors, $logErrorDetails);

	$errorHandler = $errorMiddleware->getDefaultErrorHandler();
	$errorHandler->registerErrorRenderer('application/json', JsonErrorRenderer::class);
};
