<?php

use App\action\AuthorCreateAction;
use App\action\BookCreateAction;
use App\action\MeAction;
use App\Action\PreFlightAction;
use App\Action\TokenCreateAction;
use App\Action\UserCreateAction;
use App\Middleware\JwtMiddleware;
use Slim\App;
use Slim\Exception\HttpNotFoundException;

return function (App $app) {
	$app->options('/{routes:.+}', PreFlightAction::class);
	$app->post('/register', UserCreateAction::class);
	$app->post('/login', TokenCreateAction::class);

	$app->post('/book', BookCreateAction::class);
	$app->post('/author', AuthorCreateAction::class)->add(JwtMiddleware::class);


	$app->get('/me', MeAction::class)->add(JwtMiddleware::class);

	$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH',], '/{routes:.+}', function ($request, $response) {
		throw new HttpNotFoundException($request);
	});
};
