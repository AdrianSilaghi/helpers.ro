<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class Test extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table(
            'user_profile',
            [
                'id' => false,
                'primary_key' => ['id'],
                'engine' => 'InnoDB',
                'comment' => '',
                'row_format' => 'DYNAMIC',
            ]
        )
            ->addColumn(
                'id',
                'integer',
                [
                    'null' => false,
                    'limit' => MysqlAdapter::INT_REGULAR,
                    'identity' => 'enable',
                ]
            )
            ->addColumn(
                'first_name',
                'string',
                [
                    'null' => true,
                    'limit' => 255,
                    'after' => 'id',
                ]
            )
            ->addColumn(
                'last_name',
                'string',
                [
                    'null' => true,
                    'limit' => 255,
                ]
            )
            ->addColumn(
                'birthday',
                'date',
                [
                    'null' => true,
                    'limit' => 255,
                ]
            )
            ->create();
        
        $this->table('user')->addColumn(
            'profile_id',
            'integer',
            [
                'null' => false,
                'limit' => MysqlAdapter::INT_REGULAR,
            ]
        )->update();
        
        $this->table('user')->addIndex('profile_id')->update();
    }
}
