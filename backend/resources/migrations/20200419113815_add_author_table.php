<?php

use Phinx\Db\Adapter\MysqlAdapter;
use Phinx\Migration\AbstractMigration;

class AddAuthorTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
	    $this->execute("ALTER DATABASE CHARACTER SET 'latin1';");
	    $this->execute("ALTER DATABASE COLLATE='latin1_swedish_ci';");
	    $this->table('author', [
		    'id' => false,
		    'primary_key' => ['id'],
		    'engine' => 'InnoDB',
		    'encoding' => 'utf8mb4',
		    'collation' => 'utf8mb4_unicode_ci',
		    'comment' => '',
		    'row_format' => 'DYNAMIC',
	    ])
		    ->addColumn('id', 'integer', [
			    'null' => false,
			    'limit' => MysqlAdapter::INT_REGULAR,
			    'identity' => 'enable',
		    ])
		    ->addColumn('name', 'string', [
		    ])
		    ->create();
    }
}
