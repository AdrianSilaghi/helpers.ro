<?php


namespace App\action;


use App\Domain\Author\Data\AuthorCreateData;
use App\Domain\Author\Service\AuthorCreator;
use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Data\UserProfileCreateData;
use App\Domain\User\Service\UserCreator;
use App\Domain\User\Service\UserProfileCreator;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

final class AuthorCreateAction
{
	/** @var AuthorCreator */
	private $authorCreator;

	/**
	 * UserCreateAction constructor.
	 *
	 * @param AuthorCreator $userCreator User Creator Service
	 */
	public function __construct(
		AuthorCreator $userCreator
	) {
		$this->userCreator = $userCreator;
	}

	/**
	 * @param Request $request Req
	 * @param Response $response Res
	 *
	 * @return Response
	 */
	public function __invoke(Request $request, Response $response): Response
	{
		$data = @json_decode($request->getBody()->getContents(), true);
		$id = $this->authorCreator->createAuthor(new AuthorCreateData($data));

		$result = [
			'author_id' => $id,
		];

		$response->getBody()->write(@json_encode($result));

		return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
	}
}