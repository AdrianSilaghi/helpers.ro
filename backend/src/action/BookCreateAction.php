<?php


namespace App\action;


use App\Domain\Author\Data\AuthorCreateData;
use App\Domain\Author\Service\AuthorCreator;
use App\Domain\Book\Data\BookCreateData;
use App\Domain\Book\Service\BookCreator;
use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Data\UserProfileCreateData;
use App\Domain\User\Service\UserCreator;
use App\Domain\User\Service\UserProfileCreator;
use Slim\Psr7\Request;
use Slim\Psr7\Response;

final class BookCreateAction
{
	/** @var BookCreator */
	private $bookCreator;

	/**
	 * UserCreateAction constructor.
	 *
	 * @param BookCreator $bookCreator User Creator Service
	 */
	public function __construct(
		BookCreator $bookCreator
	) {
		$this->bookCreator = $bookCreator;
	}

	/**
	 * @param Request $request Req
	 * @param Response $response Res
	 *
	 * @return Response
	 */
	public function __invoke(Request $request, Response $response): Response
	{
		$data = @json_decode($request->getBody()->getContents(), true);
		$id = $this->bookCreator->createBook(new BookCreateData($data));

		$result = [
			'book_id' => $id,
		];

		$response->getBody()->write(@json_encode($result));

		return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
	}
}