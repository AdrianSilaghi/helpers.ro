<?php


namespace App\action;


use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Repository\UserGetRepository;
use App\Domain\User\Repository\UserProfileGetRepository;
use App\Domain\User\Service\UserCreator;
use Illuminate\Validation\Factory;
use Slim\Psr7\Request as Request;
use Psr\Http\Message\ResponseInterface as Response;

final class MeAction
{
    /** @var UserGetRepository */
    private $userGetRepository;
    /** @var UserProfileGetRepository */
    private $userProfileGetRepository;
    
    /**
     * UserCreateAction constructor.
     *
     * @param UserCreator $userGetRepository User Creator Service
     */
    public function __construct(UserGetRepository $userGetRepository, UserProfileGetRepository $userProfileGetRepository)
    {
        $this->userGetRepository = $userGetRepository;
        $this->userProfileGetRepository = $userProfileGetRepository;
    }
    
    /**
     * @param Request $request Req
     * @param Response $response Res
     *
     * @return Response
     */
    public function __invoke(Request $request, Response $response): Response
    {
    	$email = $request->getAttribute('uid');
        $user = $this->userGetRepository->findByEmail($email);
        $profile = $this->userProfileGetRepository->findByPk($user->profile_id);

        $data = array_merge($user->toArray(), $profile->toArray());

        $response->getBody()->write(@json_encode($data));
        
        return $response->withHeader('Content-Type', 'application/json')->withStatus(200);
    }
}