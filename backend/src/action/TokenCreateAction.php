<?php

namespace App\Action;

use App\Auth\JwtAuth;
use App\Auth\UserAuth;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class TokenCreateAction
 * @package App\Action
 */
class TokenCreateAction
{
	/**
	 * @var JwtAuth Auth
	 */
	private $jwtAuth;

	/**
	 * @var UserAuth
	 */
	private $userAuth;

	/**
	 * TokenCreateAction constructor.
	 * @param JwtAuth $jwtAuth Auth
	 */
	public function __construct(JwtAuth $jwtAuth, UserAuth $userAuth)
	{
		$this->jwtAuth = $jwtAuth;
		$this->userAuth = $userAuth;
	}

	/**
	 * @param Request $request Req
	 * @param Response $response Res
	 * @return Response
	 * @throws \Exception
	 */
	public function __invoke(Request $request, Response $response): Response
	{
		$data = (array)$request->getParsedBody();

		$email = (string)($data['email'] ?? '');
		$password = (string)($data['password'] ?? '');

		$isValidLogin = $this->userAuth->checkLogin($email, $password);

		if (!$isValidLogin) {
			$response->getBody()->write(@json_encode($this->userAuth->getErrors()));
			return $response
				->withHeader('Content-Type', 'application/json')
				->withStatus(401, 'Unauthorized');
		}

		$token = $this->jwtAuth->createJwt($email);
		$lifetime = $this->jwtAuth->getLifetime();

		$result = [
			'access_token' => $token,
			'token_type' => 'Bearer',
			'expires_in' => $lifetime,
		];

		$response->getBody()->write(@json_encode($result));

		return $response->withStatus(201);
	}
}
