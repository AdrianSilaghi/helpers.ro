<?php

namespace App\Action;

use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Data\UserProfileCreateData;
use App\domain\user\repository\UserUpdateRepository;
use App\Domain\User\Service\UserCreator;
use App\Domain\User\Service\UserProfileCreator;
use Illuminate\Validation\Factory;
use Psr\Http\Message\RequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class UserCreateAction
 * @package App\Action
 */
final class UserCreateAction
{
	/** @var UserCreator */
	private $userCreator;

	/** @var UserProfileCreator */
	private $userProfileCreator;

	/**
	 * UserCreateAction constructor.
	 *
	 * @param UserCreator $userCreator User Creator Service
	 * @param UserProfileCreator $userProfileCreator
	 */
	public function __construct(
		UserCreator $userCreator,
		UserProfileCreator $userProfileCreator
	) {
		$this->userCreator = $userCreator;
		$this->userProfileCreator = $userProfileCreator;
	}

	/**
	 * @param Request $request Req
	 * @param Response $response Res
	 *
	 * @return Response
	 */
	public function __invoke(Request $request, Response $response): Response
	{
		$data = @json_decode($request->getBody()->getContents(), true);
		$profileId = $this->userProfileCreator->createProfile(new UserProfileCreateData($data));
		$userId = $this->userCreator->createUser(new UserCreateData(array_merge($data, ['profile_id' => $profileId])));

		$result = [
			'user_id' => $userId,
		];

		$response->getBody()->write(@json_encode($result));

		return $response->withHeader('Content-Type', 'application/json')->withStatus(201);
	}
}
