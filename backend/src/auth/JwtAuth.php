<?php

namespace App\Auth;

use Cake\Chronos\Chronos;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\ValidationData;
use Ramsey\Uuid\Uuid;

/**
 * Class JwtAuth
 * @package App\Auth
 */
final class JwtAuth
{
    /** @var string */
    private $issuer;

    /** @var int */
    private $lifetime;

    /** @var string */
    private $privateKey;

    /** @var string */
    private $publicKey;

    /** @var Sha256 */
    private $signer;

    /**
     * JwtAuth constructor.
     *
     * @param string $issuer Issuer
     * @param int $lifetime Time of session
     * @param string $privateKey Private key
     * @param string $publicKey Public key
     */
    public function __construct(
        string $issuer,
        int $lifetime,
        string $privateKey,
        string $publicKey
    ) {
        $this->issuer = $issuer;
        $this->lifetime = $lifetime;
        $this->privateKey = $privateKey;
        $this->publicKey = $publicKey;
        $this->signer = new Sha256();
    }

    /**
     * @return int
     */
    public function getLifetime(): int
    {
        return $this->lifetime;
    }

    /**
     * @param string $uid Uuid
     *
     * @throws \Exception
     *
     * @return string
     */
    public function createJwt(string $uid): string
    {
        $issuedAt = Chronos::now()->getTimestamp();

        return (new Builder())->issuedBy($this->issuer)
            ->identifiedBy(Uuid::uuid4()->toString(), true)
            ->canOnlyBeUsedAfter($issuedAt)
            ->expiresAt($issuedAt + $this->lifetime)
            ->withClaim('uid', $uid)
            ->getToken($this->signer, new Key($this->privateKey));
    }
    
    /**
     * @param string $token Token
     * @return Token
     */
    public function createParsedToken(string $token): Token
    {
        return (new Parser())->parse($token);
    }
    
    /**
     * Validate token
     * @param string $accessToken Token
     * @return bool
     */
    public function validateToken(string $accessToken): bool
    {
        $token = $this->createParsedToken($accessToken);

        if (!$token->verify($this->signer, $this->publicKey)) {
            return false;
        }

        $data = new ValidationData();
        $data->setCurrentTime(Chronos::now()->getTimestamp());
        $data->setIssuer($token->getClaim('iss'));
        $data->setId($token->getClaim('jti'));

        return $token->validate($data);
    }
}
