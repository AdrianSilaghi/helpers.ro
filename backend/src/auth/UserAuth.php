<?php

namespace App\Auth;

use App\Domain\User\Repository\UserGetRepository;

/**
 * Class UserAuth
 * @package App\Domain\Auth
 */
class UserAuth
{
    /** @var UserGetRepository  */
    public $repository;
    
    /**
     * UserAuth constructor.
     * @param UserGetRepository $userGetRepository
     */
    public function __construct(UserGetRepository $userGetRepository)
    {
        $this->repository = $userGetRepository;
    }
    
    /**
     * @param string $email Email
     * @param string $password Email
     * @return bool
     */
    public function checkLogin(string $email, string $password)
    {
        $pwHash = $this->repository->getPasswordByEmail($email);
        if (password_verify($password, $pwHash)) {
            return true;
        }
        return false;
    }

	/**
	 * @return array
	 */
    public function getErrors(): array
    {
	    return [
		    'errors' => [
			    'email' => 'Email or password is incorrect',
			    'password' => 'Email or password is incorrect',
		    ]
	    ];
    }
}