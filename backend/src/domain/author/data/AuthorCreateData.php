<?php


namespace App\Domain\Author\Data;


use Selective\ArrayReader\ArrayReader;

/**
 * Class AuthorCreateData
 * @package App\Domain\Author\Data
 */
final class AuthorCreateData
{
	use AuthorData;

	/**
	 * UserCreateData constructor.
	 *
	 * @param array $attributes Attributes to be filled
	 */
	public function __construct(array $attributes = [])
	{
		$data = new ArrayReader($attributes);

		$this->id = $data->findInt('id');
		$this->name = $data->findString('name');
	}

	/**
	 * @return array
	 */
	public function toArray(): array
	{
		return [
			'id' => $this->id,
			'email' => $this->name,
		];
	}
}