<?php


namespace App\Domain\Author\Data;

/**
 * Trait AuthorData
 * @package App\Domain\Author\Data
 */
trait AuthorData
{
	/** @var integer */
	public $id;
	/** @var string */
	public $name;
}