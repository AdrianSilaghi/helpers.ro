<?php

namespace App\Domain\Author\Repository;

use App\Domain\Author\Data\AuthorCreateData;
use App\Repository\TableName;
use Illuminate\Database\Connection;

/**
 * Class AuthorCreatorRepository
 * @package App\Domain\Author\Repository
 */
class AuthorCreatorRepository
{
    /**
     * @var Connection The db connection
     */
    private $connection;

	/**
	 * AuthorCreatorRepository constructor.
	 * @param Connection $queryFactory
	 */
    public function __construct(Connection $queryFactory)
    {
        $this->connection = $queryFactory;
    }

	/**
	 * @param AuthorCreateData $author
	 * @return int
	 */
    public function insertAuthor(AuthorCreateData $author): int
    {
        $data = $author->toArray();

        return (int)$this->connection->table(TableName::AUTHOR)->insertGetId($data);
    }
}
