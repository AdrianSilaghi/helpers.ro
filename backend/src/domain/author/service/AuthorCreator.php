<?php

namespace App\Domain\Author\Service;

use App\Domain\Author\Data\AuthorCreateData;
use App\Domain\Author\Repository\AuthorCreatorRepository;
use App\Domain\User\Repository\UserCreatorRepository;
use App\Domain\Author\Validator\AuthorValidator;
use App\Factory\LoggerFactory;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;


final class AuthorCreator
{
    /**
     * @var AuthorCreatorRepository
     */
    private $repository;
    
    /**
     * @var AuthorValidator
     */
    protected $authorValidator;
    
    /**
     * @var LoggerFactory
     */
    private $logger;
    
    /**
     * UserCreator constructor.
     * @param UserCreatorRepository $repository Repo
     */
    public function __construct(
        AuthorCreatorRepository $repository,
        AuthorValidator $userValidator,
        LoggerFactory $loggerFactory
    )
    {
        $this->repository = $repository;
        $this->userValidator = $userValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('author_creator.log')
            ->createInstance('author_creator');
    }

	/**
	 * Create a new user.
	 *
	 * @param AuthorCreateData $author The user data
	 *
	 * @return int The new user ID
	 */
    public function createAuthor(AuthorCreateData $author): int
    {
        /** @var ValidationResult $validation */
        $validation = $this->authorValidator->validateAuthor($author);

        if ($validation->isFailed()) {
            $validation->setMessage('Please check your input');
            
            throw new ValidationException($validation);
        }
        // Insert author
        $id = $this->repository->insertAuthor($author);
        $this->logger->info("Author created successfully: $id");
        return $id;
    }
}
