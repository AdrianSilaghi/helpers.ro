<?php

namespace App\Domain\Author\Validator;

use App\Domain\Author\Data\AuthorCreateData;
use Selective\Validation\ValidationResult;

/**
 * Class AuthorValidator
 * @package App\Domain\User\Validator
 */
final class AuthorValidator
{
    /**
     * Validates user
     * @param AuthorCreateData $authorCreateData User data
     * @return ValidationResult
     */
    public function validateAuthor(AuthorCreateData $authorCreateData)
    {
        $validation = new ValidationResult();
        
        if (empty($authorCreateData->name)) {
            $validation->addError('email', 'Input required');
        }

        return $validation;
    }
}