<?php


namespace App\Domain\Book\Data;


use Selective\ArrayReader\ArrayReader;

/**
 * Class BookCreateData
 * @package App\Domain\Book\Data
 */
final class BookCreateData
{
	use BookData;

	/**
	 * UserCreateData constructor.
	 *
	 * @param array $attributes Attributes to be filled
	 */
	public function __construct(array $attributes = [])
	{
		$data = new ArrayReader($attributes);

		$this->id = $data->findInt('id');
		$this->name = $data->findString('name');
		$this->description = $data->findString('description');
	}

	/**
	 * @return array
	 */
	public function toArray(): array
	{
		return [
			'name' => $this->name,
			'description' => $this->description,
		];
	}
}