<?php


namespace App\Domain\Book\Data;

/**
 * Trait BookData
 * @package App\Domain\Book
 */
trait BookData
{
	/** @var integer */
	public $id;
	/** @var string */
	public $name;
	/** @var string */
	public $description;
}