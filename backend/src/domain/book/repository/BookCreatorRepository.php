<?php

namespace App\Domain\Book\Repository;

use App\Domain\Author\Data\AuthorCreateData;
use App\Domain\Book\Data\BookCreateData;
use App\Repository\TableName;
use Illuminate\Database\Connection;

/**
 * Class BookCreatorRepository
 * @package App\Domain\Author\Repository
 */
class BookCreatorRepository
{
    /**
     * @var Connection The db connection
     */
    private $connection;

	/**
	 * BookCreatorRepository constructor.
	 * @param Connection $queryFactory
	 */
    public function __construct(Connection $queryFactory)
    {
        $this->connection = $queryFactory;
    }

	/**
	 * @param BookCreateData $book
	 * @return int
	 */
    public function insertBook(BookCreateData $book): int
    {
        $data = $book->toArray();

        return (int)$this->connection->table(TableName::BOOK)->insertGetId($data);
    }
}
