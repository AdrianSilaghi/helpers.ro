<?php

namespace App\Domain\Book\Service;

use App\Domain\Book\Repository\BookCreatorRepository;
use App\Domain\Book\Data\BookCreateData;
use App\Domain\Book\Validator\BookValidator;
use App\Factory\LoggerFactory;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;


final class BookCreator
{
    /**
     * @var BookCreatorRepository
     */
    private $repository;
    
    /**
     * @var BookValidator
     */
    protected $bookValidator;
    
    /**
     * @var LoggerFactory
     */
    private $logger;
    
    /**
     * UserCreator constructor.
     * @param BookCreatorRepository $repository Repo
     */
    public function __construct(
        BookCreatorRepository $repository,
        BookValidator $bookValidator,
        LoggerFactory $loggerFactory
    )
    {
        $this->repository = $repository;
        $this->bookValidator = $bookValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('book_creator.log')
            ->createInstance('book_creator');
    }

	/**
	 * Create a new user.
	 *
	 * @param BookCreateData $book The user data
	 *
	 * @return int The new user ID
	 */
    public function createBook(BookCreateData $book): int
    {
        /** @var ValidationResult $validation */
        $validation = $this->bookValidator->validateBook($book);

        if ($validation->isFailed()) {
            $validation->setMessage('Please check your input');
            
            throw new ValidationException($validation);
        }
        // Insert book
        $id = $this->repository->insertBook($book);
        $this->logger->info("Author created successfully: $id");
        return $id;
    }
}
