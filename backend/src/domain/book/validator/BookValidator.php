<?php

namespace App\Domain\Book\Validator;

use App\Domain\Book\Data\BookCreateData;
use Selective\Validation\ValidationResult;

/**
 * Class AuthorValidator
 * @package App\Domain\User\Validator
 */
final class BookValidator
{
	/**
	 * Validates user
	 * @param BookCreateData $authorCreateData User data
	 * @return ValidationResult
	 */
    public function validateBook(BookCreateData $authorCreateData)
    {
        $validation = new ValidationResult();
        
        if (empty($authorCreateData->name)) {
            $validation->addError('email', 'Input required');
        }

        return $validation;
    }
}