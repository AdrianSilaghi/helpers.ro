<?php

namespace App\Domain\User\Data;

use Selective\ArrayReader\ArrayReader;

/**
 * Class UserCreateData
 * @package App\Domain\User\Data
 */
final class UserCreateData
{
    use UserData;

    /**
     * UserCreateData constructor.
     *
     * @param array $attributes Attributes to be filled
     */
    public function __construct(array $attributes = [])
    {
        $data = new ArrayReader($attributes);
        
        $this->id = $data->findInt('id');
        $this->email = $data->findString('email');
        $this->password = $data->findString('password');
        $this->enabled = $data->findBool('enabled');
        $this->profile_id = $data->findInt('profile_id');
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'profile_id' => $this->profile_id
        ];
    }
}
