<?php


namespace App\Domain\User\Data;

/**
 * Trait UserData
 * @package App\Domain\User\Data
 */
trait UserData
{
    /** @var integer|null */
    public $id;
    /** @var string */
    public $email;
    /** @var string */
    public $password;
    /** @var bool  */
    public $enabled = false;
    /** @var integer */
    public $profile_id;
}