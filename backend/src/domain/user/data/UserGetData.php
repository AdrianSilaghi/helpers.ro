<?php


namespace App\Domain\User\Data;


use App\Domain\User\Data\UserData;
use Selective\ArrayReader\ArrayReader;

/**
 * Class UserGetData
 * @package App\domain\user\data
 */
final class UserGetData
{
    use UserData;
    
    /**
     * UserCreateData constructor.
     *
     * @param array $attributes Attributes to be filled
     */
    public function __construct(array $attributes = [])
    {
        $data = new ArrayReader($attributes);
        
        $this->id = $data->findInt('id');
        $this->email = $data->findString('email');
        $this->enabled = $data->findBool('enabled');
        $this->profile_id = $data->findInt('profile_id');
    }
    
    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'email' => $this->email,
            'password' => $this->password,
            'enabled' => $this->enabled,
        ];
    }
}