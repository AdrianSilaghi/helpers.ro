<?php


namespace App\Domain\User\Data;

/**
 * Trait UserProfileData
 * @package App\Domain\User\Data
 */
trait UserProfileData
{
    /** @var integer|null */
    public $id;
    /** @var string */
    public $first_name;
    /** @var string */
    public $last_name;
    /** @var string */
    public $birthday;
}