<?php


namespace App\Domain\User\Data;


use App\Domain\User\Data\UserProfileData;
use Selective\ArrayReader\ArrayReader;

/**
 * Class UserProfileGetData
 * @package App\Domanin\User\Data
 */
final class UserProfileGetData
{
    use UserProfileData;
    
    /**
     * UserCreateData constructor.
     *
     * @param array $attributes Attributes to be filled
     */
    public function __construct(array $attributes = [])
    {
        $data = new ArrayReader($attributes);
        
        $this->id = $data->findInt('id');
        $this->first_name = $data->findString('first_name');
        $this->last_name = $data->findString('last_name');
        $this->birthday = $data->findString('birthday');
    }
    
    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'birthday' => $this->birthday,
        ];
    }
}