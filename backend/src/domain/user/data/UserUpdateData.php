<?php


namespace App\Domain\User\Data;


use Selective\ArrayReader\ArrayReader;

/**
 * Class UserUpdateData
 * @package App\domain\user\data
 */
final class UserUpdateData
{
    use UserData;
    
    /**
     * UserCreateData constructor.
     *
     * @param array $attributes Attributes to be filled
     */
    public function __construct(array $attributes = [])
    {
        $data = new ArrayReader($attributes);
        
        $this->id = $data->findInt('id');
        $this->email = $data->findString('email');
        $this->password = $data->findString('password');
        $this->profile_id = $data->findInt('profile_id');
    }
    
    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'password' => $this->password,
            'profile_id' => $this->profile_id,
        ];
    }
}