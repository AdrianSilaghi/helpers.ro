<?php

namespace App\Domain\User\Repository;

use App\Domain\User\Data\UserCreateData;
use App\Repository\TableName;
use Illuminate\Database\Connection;

/**
 * Class UserCreatorRepository.
 *
 * @property Connection $connection
 */
class UserCreatorRepository
{
    /**
     * @var Connection The db connection
     */
    private $connection;

    /**
     * UserCreatorRepository constructor.
     *
     * @param Connection $queryFactory Conn
     */
    public function __construct(Connection $queryFactory)
    {
        $this->connection = $queryFactory;
    }

    /**
     * Insert user in row.
     *
     * @param UserCreateData $user User
     *
     * @return int
     */
    public function insertUser(UserCreateData $user): int
    {
        $data = $user->toArray();
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        
        return (int)$this->connection->table(TableName::USER)->insertGetId($data);
    }
}
