<?php


namespace App\Domain\User\Repository;


use App\Repository\TableName;
use Illuminate\Database\Connection;

class UserExistRepository
{
    /**
     * @var Connection The db connection
     */
    private $connection;
    
    /**
     * UserCreatorRepository constructor.
     *
     * @param Connection $connection Conn
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
    
    /**
     * Check if user already exists
     * @param string $email Email
     * @return bool
     */
    public function existsByEmail(string $email): bool
    {
        return (bool)$this->connection->table(TableName::USER)->where('email', '=', $email)->exists();
    }
}