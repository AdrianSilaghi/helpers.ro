<?php


namespace App\Domain\User\Repository;


use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Data\UserGetData;
use App\Repository\TableName;
use Illuminate\Database\Connection;

class UserGetRepository
{
    /**
     * @var Connection The db connection
     */
    private $connection;
    
    /**
     * UserCreatorRepository constructor.
     *
     * @param Connection $connection Conn
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
    
    /**
     * @param string $email Email
     * @return UserCreateData
     */
    public function findByEmail(string $email): UserCreateData
    {
    	$data = (array) $this->connection->table(TableName::USER)->where("email", "=", $email)->get()[0];
        return new UserCreateData($data);
    }
    
    /**
     * Finds and
     *
     * @param $pk
     * @return UserCreateData
     */
    public function findByPk($pk): UserGetData
    {
        $data = (array) $this->connection->table(TableName::USER)->where("id", "=", $pk)->get();
        return new UserGetData($data);
    }
    
    /**
     * Returns hashed pw by email
     * @param string $email Email
     * @return string
     */
    public function getPasswordByEmail(string $email): string
    {
        return $this->connection
            ->table(TableName::USER)
            ->where('email', '=', $email)->get(['password'])[0]->password ?? '';
    }
}