<?php

namespace App\Domain\User\Repository;

use App\Domain\User\Data\UserProfileCreateData;
use App\Repository\TableName;
use Illuminate\Database\Connection;

/**
 * Class UserProfileCreatorRepository
 * @package App\Domain\User\Repository
 */
class UserProfileCreatorRepository
{
    /**
     * @var Connection The db connection
     */
    private $connection;

    /**
     * UserCreatorRepository constructor.
     *
     * @param Connection $queryFactory Conn
     */
    public function __construct(Connection $queryFactory)
    {
        $this->connection = $queryFactory;
    }

    /**
     * Insert user profile in row.
     *
     * @param UserProfileCreateData $userProfileCreateData UserProfile
     *
     * @return int
     */
    public function insert(UserProfileCreateData $userProfileCreateData): int
    {
        $data = $userProfileCreateData->toArray();
        
        return (int)$this->connection->table(TableName::USER_PROFILE)->insertGetId($data);
    }
}
