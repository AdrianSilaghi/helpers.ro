<?php


namespace App\Domain\User\Repository;


use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Data\UserProfileGetData;
use App\Repository\TableName;
use Illuminate\Database\Connection;

class UserProfileGetRepository
{
	/**
	 * @var Connection The db connection
	 */
	private $connection;

	/**
	 * UserCreatorRepository constructor.
	 *
	 * @param Connection $connection Conn
	 */
	public function __construct(Connection $connection)
	{
		$this->connection = $connection;
	}

	/**
	 * Finds and
	 *
	 * @param $pk
	 * @return UserCreateData
	 */
	public function findByPk($pk): UserProfileGetData
	{
		$data = (array) $this->connection->table(TableName::USER_PROFILE)->where("id", "=", $pk)->get()[0];
		return new UserProfileGetData($data);
	}
}