<?php


namespace App\domain\user\repository;


use App\Domain\User\Data\UserCreateData;
use App\domain\user\data\UserUpdateData;
use App\Repository\TableName;
use Illuminate\Database\Connection;

final class UserUpdateRepository
{
    /**
     * @var Connection The db connection
     */
    private $connection;
    
    /**
     * UserCreatorRepository constructor.
     *
     * @param Connection $queryFactory Conn
     */
    public function __construct(Connection $queryFactory)
    {
        $this->connection = $queryFactory;
    }
    
    /**
     * Insert user in row.
     *
     * @param UserCreateData $user User
     *
     * @return int
     */
    public function update(UserUpdateData $user): int
    {
        $data = $user->toArray();
        
        return (int)$this->connection->table(TableName::USER)->update($data);
    }
}