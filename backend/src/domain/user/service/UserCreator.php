<?php

namespace App\Domain\User\Service;

use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Repository\UserCreatorRepository;
use App\Domain\User\Validator\UserValidator;
use App\Factory\LoggerFactory;
use mysql_xdevapi\Exception;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;

/**
 * Class UserCreator.
 *
 * @property UserCreatorRepository $repository
 */
final class UserCreator
{
    /**
     * @var UserCreatorRepository
     */
    private $repository;
    
    /**
     * @var UserValidator
     */
    protected $userValidator;
    
    /**
     * @var LoggerFactory
     */
    private $logger;
    
    /**
     * UserCreator constructor.
     * @param UserCreatorRepository $repository Repo
     */
    public function __construct(
        UserCreatorRepository $repository,
        UserValidator $userValidator,
        LoggerFactory $loggerFactory
    )
    {
        $this->repository = $repository;
        $this->userValidator = $userValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('user_creator.log')
            ->createInstance('user_creator');
    }
    
    /**
     * Create a new user.
     *
     * @param UserCreateData $user The user data
     *
     * @return int The new user ID
     */
    public function createUser(UserCreateData $user): int
    {
        /** @var ValidationResult $validation */
        $validation = $this->userValidator->validateUser($user);

        if ($validation->isFailed()) {
            $validation->setMessage('Please check your input');
            
            throw new ValidationException($validation);
        }
        // Insert user
        $userId = $this->repository->insertUser($user);
        
        $this->logger->info("User created successfully: $userId");
        
        // Logging here: User created successfully
        
        return $userId;
    }
}
