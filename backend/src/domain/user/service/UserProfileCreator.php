<?php

namespace App\Domain\User\Service;

use App\Domain\User\Data\UserProfileCreateData;
use App\Domain\User\Repository\UserProfileCreatorRepository;
use App\Domain\User\Validator\UserProfileValidator;
use App\Domain\User\Validator\UserValidator;
use App\Factory\LoggerFactory;
use Selective\Validation\Exception\ValidationException;
use Selective\Validation\ValidationResult;

/**
 * Class UserProfileCreator
 * @package App\Domain\User\Service
 */
final class UserProfileCreator
{
    /**
     * @var UserProfileCreatorRepository
     */
    private $repository;
    
    /**
     * @var UserProfileValidator
     */
    protected $validator;
    
    /**
     * @var LoggerFactory
     */
    private $logger;
    
    /**
     * UserProfileCreator constructor.
     * @param UserProfileCreatorRepository $repository
     * @param UserProfileValidator $userValidator
     * @param LoggerFactory $loggerFactory
     */
    public function __construct(
        UserProfileCreatorRepository $repository,
        UserProfileValidator $userValidator,
        LoggerFactory $loggerFactory
    )
    {
        $this->repository = $repository;
        $this->validator = $userValidator;
        $this->logger = $loggerFactory
            ->addFileHandler('user_profile_creator.log')
            ->createInstance('user_profile_creator');
    }
    
    /**
     * Create a new user profile
     *
     * @param UserProfileCreateData $user The user data
     *
     * @return int The new user ID
     */
    public function createProfile(UserProfileCreateData $user): int
    {
        /** @var ValidationResult $validation */
        $validation = $this->validator->validate($user);

        if ($validation->isFailed()) {
            $validation->setMessage('Please check your input');
            
            throw new ValidationException($validation);
        }
        // Insert user profile
        $profileId = $this->repository->insert($user);
        
        $this->logger->info("User profile created successfully: $profileId");
        
        // Logging here: User created successfully
        
        return $profileId;
    }
}
