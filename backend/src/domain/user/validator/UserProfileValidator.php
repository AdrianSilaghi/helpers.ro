<?php

namespace App\Domain\User\Validator;

use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Data\UserProfileCreateData;
use App\Domain\User\Repository\UserGetRepository;
use Selective\Validation\ValidationResult;

final class UserProfileValidator
{
    /**
     * Validates user
     * @param UserProfileCreateData $userProfileCreateData
     * @return ValidationResult
     */
    public function validate(UserProfileCreateData $userProfileCreateData)
    {
        $validation = new ValidationResult();
        
        if (empty($userProfileCreateData->first_name)) {
            $validation->addError('first_name', 'Input required');
        }
    
        if (empty($userProfileCreateData->last_name)) {
            $validation->addError('last_name', 'Input required');
        }
    
        if (empty($userProfileCreateData->birthday)) {
            $validation->addError('birthday', 'Input required');
        }
        
        return $validation;
    }
}