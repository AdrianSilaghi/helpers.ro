<?php

namespace App\Domain\User\Validator;

use App\Domain\User\Data\UserCreateData;
use App\Domain\User\Repository\UserExistRepository;
use App\Domain\User\Data\UserGetData;
use Selective\Validation\ValidationResult;

final class UserValidator
{
    /** @var UserExistRepository */
    public $userExistRepository;
    
    public function __construct(UserExistRepository $userExistRepository)
    {
        $this->userExistRepository = $userExistRepository;
    }
    
    /**
     * Validates user
     * @param UserCreateData $userCreateData User data
     * @return ValidationResult
     */
    public function validateUser(UserCreateData $userCreateData)
    {
        $validation = new ValidationResult();
        
        if (empty($userCreateData->email)) {
            $validation->addError('email', 'Input required');
        }
        
        if (filter_var($userCreateData->email, FILTER_VALIDATE_EMAIL) === false) {
            $validation->addError('email', 'Invalid email address');
        }
        
        if ($this->userExistRepository->existsByEmail($userCreateData->email)) {
            $validation->addError('email', 'Email is already in use');
        }
        
        if (empty($userCreateData->password)) {
            $validation->addError('password', 'Input required');
        }
        
        return $validation;
    }
	/**
     * @param UserGetData $userGetData
     * @return ValidationResult
     */
    public function validateId(UserGetData $userGetData)
    {
        $validation = new ValidationResult();
    
        if (empty($userGetData->id)) {
            $validation->addError('id', 'Input required');
        }
        
        return $validation;
    }
}