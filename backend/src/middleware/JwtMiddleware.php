<?php

namespace App\Middleware;

use App\Auth\JwtAuth;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class JwtMiddleware
 * @package App\Middleware
 */
final class JwtMiddleware implements MiddlewareInterface
{
    /** @var JwtAuth */
    private $jwtAuth;
    
    /** @var ResponseFactoryInterface */
    private $responseFactory;
    
    /**
     * JwtMiddleware constructor.
     * @param JwtAuth $jwtAuth Auth
     * @param ResponseFactoryInterface $responseFactory Factory
     */
    public function __construct(JwtAuth $jwtAuth, ResponseFactoryInterface $responseFactory)
    {
        $this->jwtAuth = $jwtAuth;
        $this->responseFactory = $responseFactory;
    }
    
    /**
     * @param ServerRequestInterface $request Request
     * @param RequestHandlerInterface $handler Handler
     * @return ResponseInterface Response
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $authorization = explode(' ', (string)$request->getHeaderLine('Authorization'));
        $token = $authorization[1] ?? '';
        
        if (!$token || !$this->jwtAuth->validateToken($token)) {
            return $this->responseFactory->createResponse()
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(401, 'Unauthorized');
        }
        
        // Append valid token
        $parsedToken = $this->jwtAuth->createParsedToken($token);
        $request = $request->withAttribute('token', $parsedToken);
        
        // Append the user id as request attribute
        $request = $request->withAttribute('uid', $parsedToken->getClaim('uid'));
        
        return $handler->handle($request);
    }
}
