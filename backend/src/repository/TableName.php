<?php

namespace App\Repository;

class TableName
{
    public const USER = 'user';
    public const USER_PROFILE = 'user_profile';
    public const AUTHOR = 'author';
    public const BOOK = 'book';
}