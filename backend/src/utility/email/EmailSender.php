<?php


namespace App\Utility\Email;

/**
 * Class EmailSender
 * @package App\Utility\Email
 */
final class EmailSender
{
    private $mailer;
    private $from = 'no-reply@helpers.ro';
    private $message;
    
    /**
     * EmailSender constructor.
     */
    public function __construct()
    {
        $transport = (new \Swift_SmtpTransport('smtp.mailtrap.io', '25'))
            ->setUsername('bb8fe5f375e8ee')
            ->setPassword('62b1beacfd5a44');
        $this->mailer = new \Swift_Mailer($transport);
    }
    
    /**
     * @param string $subject
     * @param string $body
     * @param string $to
     * @return $this
     */
    public function createMessage(string $subject, string $body, string $to)
    {
        $this->message = (new \Swift_Message($subject))
            ->setFrom($this->from)
            ->setTo($to)
            ->setBody($body);
        return $this;
    }
    
    /**
     * @return int
     */
    public function send()
    {
        return $this->mailer->send($this->message);
    }
}