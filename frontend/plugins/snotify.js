import Vue from "vue";
import Snotify from 'vue-snotify';
import VueI18n from "vue-i18n";

// Tell Vue to use our plugin
Vue.use(Snotify);


export default ({ app }) => {
  app.snotify = Snotify;
};
