import {confirmed, email, max, min, required} from "vee-validate/dist/rules";
import {extend, setInteractionMode, configure} from 'vee-validate';

export default ({app}) => {
  configure({
    defaultMessage: (field, values) => {
      // override the field name.
      values._field_ = app.i18n.t(`fields.${field}`);

      return app.i18n.t(`validation.${values._rule_}`, values);
    }
  });

  setInteractionMode('eager');
  extend('required', required);
  extend('confirmed', confirmed);
  extend('max', max);
  extend('min', min);
  extend('email', email);
}
