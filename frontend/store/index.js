export const state = () => ({
  locales: ['en', 'ro'],
  locale: 'ro',
});

export const getters = {
  isAuthenticated: state => {
    return state.auth.loggedIn
  },
  loggedInUser: state => {
    return state.auth.user
  }
};

export const mutations = {
  SET_LANG (state, locale) {
    if (state.locales.indexOf(locale) !== -1) {
      state.locale = locale
    }
  }
};
